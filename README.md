# GeradorHTML_JSON_DOC_Postman

Este projeto é o seguinte, criei uma documentação de minha api com o Postman, então ele me forneceu somente alguns arquivos .json (JSONS parecidos com o Exemplo.json), então precisava gerar uma página html daquele arquivo json. Pesquisando encontrei um código que pode fazer isso, criei uma função do método encontrado. E então criei uma interface gráfica disso tudo. E sim, este projeto é um "Frankenstein" de meus conhecimentos com a linguagem com o ChatGPT da OpenAI. E fiz isso pois as outras formas de fazer isso geralmente eram pagos e como estou Liso, Leso e Louco eu decidi por pedir ajuda á ChatGPT e que ao meu ver ficou muito bom.

# Tá mas como faço para executar isso?

Imagino que você já esteja na página do projeto no gitlab, então clique em "clone", clicando aparecerá informações como quer clonar o projeto, selecione ou ssh, ou https ou outra opção que quiser. Selecionado sua opção abra o seu terminal, (pode ser a sua pasta de usuário) e digite "git clone git@gitlab.com:boirods/geradorhtml_json_doc_postman.git" (no meu caso selecionei ssh) e ele gerará toda a estrutura do projeto em seu computador, acesse a pasta raiz do projeto, e digite "python setup.py build" e isto deve gerar um novo arquivo para sua arquitetura de processador, em meu caso gerou na pasta build as seguinte pasta:
~/geradorhtml_json_doc_postman/build/exe.linux-x86_64-3.9/

E ela contem a pasta das libs e a pasta do template, procure mante-las. E ainda no terminal você pode executar o arquivo generate como estou no linux é dessa forma "./generate". Ou você poderia navegar até esta pasta acima informada e clicar duas vezes em generate que funciona também...

# Agora a minha avaliação sobre o projeto.

Bom este projeto de gerar a documentação pelo Postman, e criar o aplicativo, e adicionar os arquivos gerados por ambos em nossa api (sim fiz isso, integrei uma pasta docs/ á minha api) pra mim que sou meio que imediatista, ele demorou muito (que na verdade são menos de 3 dias kkkk). Perguntando algumas coisas sobre isso ao ChatGPT eu estava desde antes de ontem (dia 24/02) mas só hoje passei mais de 9 horas perguntando sobre como criar a interface gráfica do aplicativo (o que foi relativamente rápido), mas o que mais me demorou (ao meu ver) foi sobre como exibir esses arquivos (que coloquei na pasta docs na raiz do projeto) na minha aplicação Flask, demorou pois ele não sabia como era a estrutura dos arquivos do meu projeto, etc e etc. Mas agora que tenho este aplicativo creio que será mais rápido (o que é melhor que pagar kkkk). E se quiser saber como ficou o projeto do sistema completo que citei, ele é este <a href="https://gitlab.com/boirods/bookman-flask">Bookman-Flask</a>.
Então resumindo foi demorado pra colocar tudo em ordem, mas deu certo no final kkkk.
