import sys
from cx_Freeze import setup, Executable

# Dependências do projeto
build_exe_options = {"packages": ["os", "tkinter", "json", "jinja2"], "include_files":["html_template/"]}

# Informações do executável
executables = [
    Executable(script="generate.py", base=None)
]

setup(
    name="FromPostmanDOCJSON2HTML",
    version="0.1",
    description="Gera arquivos html de arquivos da documentação de sua api feita com o Postman, o postman nos da somente um arquivo JSON e então criei este aplicativo para isto. Pois todas as opções que encontrei eram pagas...",
    options={"build_exe": build_exe_options},
    executables=executables,
)
