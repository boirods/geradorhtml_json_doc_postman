import json
import os
import tkinter as tk
from tkinter import filedialog

from jinja2 import Template

# Importe a função generate_html que você criou

def select_folder():
    # Abre uma caixa de diálogo para selecionar uma pasta
    folder_path = filedialog.askdirectory()
    # Chama a função generate_html para gerar os arquivos HTML
    generate_html(folder_path)
    # Cria a label para mostrar o caminho selecionado
    folder_label = tk.Label(root, text=f'Pasta selecionada: {folder_path}')
    folder_label.pack()
    # Cria o botão para gerar o index.html
    index_button = tk.Button(root, text='Gerar códigos html', command=lambda: create_index_html(folder_path))
    index_button.pack()

def generate_html(folder_path):
    # Percorre todos os arquivos na pasta e subpastas
    for root, dirs, files in os.walk(folder_path):
        for file in files:
            # Verifica se o arquivo é um JSON
            if file.endswith('.json'):
                # Carrega os dados do arquivo JSON gerado pelo Postman
                with open(os.path.join(root, file), 'r') as f:
                    data = json.load(f)

                # Extrai os dados do JSON
                collection_name = data['info']['name']
                collection_description = data['info']['description']
                items = data['item']

                # Renderiza o template usando a biblioteca Jinja2
                with open('html_template/template.html', 'r') as f:
                    template = Template(f.read())
                    output = template.render(
                        collection_name=collection_name,
                        collection_description=collection_description,
                        items=items
                    )

                # Salva o HTML gerado em um arquivo
                output_file = os.path.splitext(file)[0] + '.html'
                output_path = os.path.join(root, output_file)
                with open(output_path, 'w') as f:
                    f.write(output)

def create_index_html(folder_path):
  # Lista os arquivos HTML na pasta especificada
  html_files = [file for file in os.listdir(folder_path) if file.endswith('.html')]

  # Cria o conteúdo do arquivo index.html com um link para cada arquivo HTML
  links_html = ""
  for file_name in html_files:
    name = os.path.splitext(file_name)[-2]+''+os.path.splitext(file_name)[-1]
    links_html += f'<li><a href="{file_name}">{name}</a></li>'

    index_html = f"""
      <!DOCTYPE html>
      <html>
        <head>
          <title>Documentação</title>
          <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        </head>
        <body>
          <div class="w3-container w3-teal">
            <h1>Documentação</h1>
          </div>
          <ul class="w3-ul">
            {links_html}
          </ul>
        </body>
      </html>"""
    # Salva o conteúdo no arquivo index.html
    with open(os.path.join(folder_path, 'index.html'), 'w') as f:
        f.write(index_html)

# Cria a janela principal
root = tk.Tk()
root.title('Gerador de HTML')

# Cria um botão para selecionar a pasta
button = tk.Button(root, text='Selecionar pasta', command=select_folder)
button.pack()

# Inicia o loop principal da interface gráfica
root.mainloop()
